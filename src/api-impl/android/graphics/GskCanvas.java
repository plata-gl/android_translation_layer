package android.graphics;

/**
 * GskCanvas:
 *   - implements Canvas for onscreen rendering inside GTKs snapshot function
 */
public class GskCanvas extends Canvas {
	private long snapshot;
	private int save_count = 0;

	public GskCanvas(long snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public int save() {
		native_save(snapshot);
		return save_count++;
	}

	@Override
	public void restore() {
		save_count--;
		native_restore(snapshot);
	}

	@Override
	public void restoreToCount(int count) {
		while (save_count > count) {
			restore();
		}
	}

	@Override
	public void translate(float dx, float dy) {
		native_translate(snapshot, dx, dy);
	}

	@Override
	public void rotate(float degrees) {
		native_rotate(snapshot, degrees);
	}

	@Override
	public void drawBitmap(Bitmap bitmap, Rect src, Rect dst, Paint paint) {
		int color = 0;
		if (paint != null && paint.colorFilter instanceof PorterDuffColorFilter) {
			color = ((PorterDuffColorFilter) paint.colorFilter).getColor();
		}
		native_drawBitmap(snapshot, bitmap.pixbuf, dst.left, dst.top, dst.width(), dst.height(), color);
	}

	@Override
	public void drawPath(Path path, Paint paint) {
		native_drawPath(snapshot, path.mNativePath, paint.skia_paint);
	}

	@Override
	public void drawRect(float left, float top, float right, float bottom, Paint paint) {
		native_drawRect(snapshot, left, top, right, bottom, paint.getColor());
	}

	@Override
	public void rotate(float degrees, float px, float py) {
		System.out.println("GskCanvas.rotate(" + degrees + ", " + px + ", " + py + ")");
	}

	@Override
	public void drawText(String text, float x, float y, Paint paint) {
		System.out.println("GskCanvas.drawText(" + text + ", " + x + ", " + y + ", " + paint + ")");
	}

	@Override
	public void drawLine(float startX, float startY, float stopX, float stopY, Paint paint) {
		System.out.println("GskCanvas.drawLine(" + startX + ", " + startY + ", " + stopX + ", " + stopY + ", " + paint + ")");
	}

	@Override
	public void drawBitmap(Bitmap bitmap, float left, float top, Paint paint) {
		Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		Rect dst = new Rect((int)left, (int)top, (int)left + bitmap.getWidth(), (int)top + bitmap.getHeight());
		drawBitmap(bitmap, src, dst, paint);
	}

	protected native void native_drawBitmap(long snapshot, long pixbuf, int x, int y, int width, int height, int color);
	protected native void native_drawRect(long snapshot, float left, float top, float right, float bottom, int color);
	protected native void native_drawPath(long snapshot, long path, long paint);
	protected native void native_translate(long snapshot, float dx, float dy);
	protected native void native_rotate(long snapshot, float degrees);
	protected native void native_save(long snapshot);
	protected native void native_restore(long snapshot);
}
